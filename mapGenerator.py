import folium
from osgeo import gdal

from pyproj import Transformer
import sys
import random

from coordinates import CoordinatesMatrix



def shapefile2geojson(infile, outfile, fieldname):
    '''Translate a shapefile to GEOJSON.
       Similar to: ogr2ogr -t_srs EPSG:4326 -f GeoJSON file.json file.shp
    '''
    options = gdal.VectorTranslateOptions(format="GeoJSON",
                                          dstSRS="EPSG:4326")

    gdal.VectorTranslate(outfile, infile, options=options)
    print("Translated GEOJSON file", outfile)

def random_html_color():
    r = random.randint(0,256)
    g = random.randint(0,256)
    b = random.randint(0,256)
    return '#%02x%02x%02x' % (r, g, b)

def create_map2(geojsom_map, lat, lon):
    '''Create a map and write it to index.html
    '''
    m = folium.Map(location=[lon, lat], zoom_start=10)


    # Add some alternate tile layers
    folium.TileLayer('Stamen Terrain').add_to(m)
    folium.TileLayer('Stamen Toner').add_to(m)

    def style_fcn(x):
        '''The style function can key off x['properties']['NAME10']
           which will be strings like 'Senate District 42'
           but for now, let's just return random colors.
        '''
        return { 'fillColor': random_html_color() }

    def highlight_fcn(x):
        return { 'fillColor': '#ff0000' }

    gj = folium.GeoJson(data=geojsom_map,
                        name="State Boundaries",
                        style_function=style_fcn,
                        highlight_function=highlight_fcn)
    gj.add_to(m)
    folium.LayerControl().add_to(m)

    m.save('index.html')
    print("Saved to index.html")

if __name__ == '__main__':
    # Usage: polidistmap senate_districts.json Senate 34.3 -105.96 NAME

    '''if not infile.lower().endswith('json'):
        jsonfile = '%s.json' % label
        shapefile2geojson(infile, jsonfile, fieldname)
        infile = jsonfile'''
    matr = CoordinatesMatrix.fromTablesCSV('data/coordinates_2.csv')
    geojsom_map, lat, lon = matr.to_GeoJSON()
    #create_map(lat, lon, label, infile, fieldname)

    create_map2(geojsom_map, lat, lon)
