import csv
import re

import geojson

import utilites
from geojson import Feature, Point, FeatureCollection


class CoordinatesMatrix:
    '''
    класс накопитель для координат
    метод ftomTables предназначен для заполнения списка координат из таблицы
    '''
    def __init__(self, list_coordinates):
        self.list_coordinates = list_coordinates

    @classmethod
    def fromTables(cls, table_csv):
        return cls(table_csv)

    @classmethod
    def fromTablesCSV(cls, file_path):
        table_coord = list()
        with open(file_path, 'r') as csvfile:
            # creating a csv reader object
            csvreader = csv.reader(csvfile)
            for row in csvreader:
                if len(row)>0:
                    try:
                        if len(str(int(row[0]))) == len(row[0]):
                            table_coord.append(utilites.convert_coordinates_2_long_lat(row[1],row[2]))
                    except ValueError as ex:
                        pass
                dd =0
            dd = 0
        return cls(table_coord)

    def to_GeoJSON(self):
        list_data = list()
        for number, el in enumerate(self.list_coordinates):
            point = Point((el[0], el[1]))
            feature = Feature(id=number, geometry=point, properties={"point": "{0}-{1}".format(el[0], el[1])})
            list_data.append(feature)
        feat_col = FeatureCollection(list_data)
        end_data = list()
        for el in self.list_coordinates:
            end_data.append((el[0], el[1]))

        polig = geojson.Polygon([end_data,])
        #return geojson.dumps(feat_col)
        print(polig.coordinates[0][0])
        print(polig.coordinates[0][0][1],polig.coordinates[0][0][0])
        return geojson.dumps(polig), polig.coordinates[0][0][0],polig.coordinates[0][0][1]
