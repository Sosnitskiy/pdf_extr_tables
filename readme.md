install Ghostscript 

    https://www.ghostscript.com/releases/gsdnld.html

install ImageMagick

    https://imagemagick.org/script/download.php

create virtual env

    python3 -m venv venv

activate venv

    .\env\Scripts\activate

install all requirements

    pip install -r requirements.txt

run

    python main.py

programm list current dirs, find all pdf files and generate folders by file name with subfolders as tables and pictures. 
in folders will be extracted all tables and pictures with table detection  


pip install GDAL==$(gdal-config --version | awk -F'[.]' '{print $1"."$2}')