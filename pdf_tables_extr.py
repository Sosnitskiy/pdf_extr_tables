import pdfplumber
import datetime
import os
import csv
import sys
import logging

logging.getLogger().setLevel(logging.INFO)

def create_folders_structure(file_name, folders):
    folder_list = list()
    name_file_l = file_name.split(".")[:-1]
    name_file = ".".join(name_file_l)
    folder_list.append(name_file)

    for folder in folders:
        folder_list.append(os.path.join(name_file, folder))
    for name_folder in folder_list:
        _create_folder_if_exist(name_folder)
    return name_file


def _create_folder_if_exist(name_folder):
    os.makedirs(name_folder, exist_ok=True)


def list_files(path=None):
    if not path:
        path = os.path.abspath(os.getcwd())
    for x in os.listdir(path):
        if x.endswith(".pdf"):
            yield x


def extract_data(pdf_file_name, folder, tables_folder='', pict_folder=''):
    table_settings = {
        "vertical_strategy": "lines",
        "horizontal_strategy": "text",
        "snap_y_tolerance": 5,
        "intersection_x_tolerance": 15,
    }
    with pdfplumber.open(pdf_file_name) as pdf:
        for page in pdf.pages:
            logging.info("Page {}".format(page.page_number))
            deb_data = page.debug_tablefinder()
            if deb_data.tables:
                today = datetime.datetime.now()
                stamp_today = today.strftime("%Y%m%d_%H%M%S")
                if sys.platform != 'win32':
                    file_name = os.path.join(folder, pict_folder, 'Page{0}_image.jpg'.format(page.page_number))
                    im = page.to_image()
                    im = im.debug_tablefinder(table_settings)
                    #im = im.debug_tablefinder()
                    im.save(file_name, bitmap_format='jpg')
                #list_tables = page.extract_tables(table_settings)
                list_tables = page.extract_tables()
                for num, table in enumerate(list_tables):
                    file_name = os.path.join(folder, tables_folder, 'Page{0}_Table{1}.csv'.format(page.page_number, num))
                    with open(file_name, 'w', newline='', encoding='utf-8') as f:
                        write = csv.writer(f,)
                        for row in table:
                            write.writerow(row)
                        #write.writerows(table)



if __name__ == '__main__':
    for file_name in list_files():
        logging.info('='*80)
        logging.info(' ======== PDF file {} ========'.format(file_name))
        root_folder = create_folders_structure(file_name, ['tables', 'pict'])
        extract_data(file_name, root_folder, tables_folder='tables', pict_folder='pict' )
        logging.info('=' * 80)

