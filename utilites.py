import re
from osgeo.osr import SpatialReference, CoordinateTransformation


def convert_coordinates_2_long_lat(value1, value2):
    regexp_temp = [r'((([0-9]?[1-8]?[0-9]?|[1-8]?[0-9]?)\D+([1-6]?[0-9]|60)\D+([0-5]?[0-9]|60)(\.[0-9]+)?|90\D+0\D+0)\D+)',]
    for regexp in regexp_temp:
        if re.search(regexp, value1):
            val1 = dms2dec(value2)
            val2 = dms2dec(value1)
            break
        else:
            epsg4284 = SpatialReference()
            #epsg4284.ImportFromEPSG(4284)
            epsg4284.ImportFromEPSG(2536)
            # Define the wgs84 system (EPSG 4326)
            epsg4326 = SpatialReference()
            epsg4326.ImportFromEPSG(4326)
            rd2latlon = CoordinateTransformation(epsg4284, epsg4326)
            lonlatz = rd2latlon.TransformPoint(float(value1), float(value2))
            val1 = lonlatz[1]
            val2 = lonlatz[0]
    return ([val1, val2])


def dms2dec(dms_str):
    """Return decimal representation of DMS
    >>> dms2dec(utf8(48°53'10.18"N))
    48.8866111111F
    >>> dms2dec(utf8(2°20'35.09"E))
    2.34330555556F
    >>> dms2dec(utf8(48°53'10.18"S))
    -48.8866111111F
    >>> dms2dec(utf8(2°20'35.09"W))
    -2.34330555556F
    """
    # Clean all whitespaces
    dms_str = re.sub(r'\s', '', dms_str)
    if re.search('[swSWoO-]', dms_str):
        sign = -1
    else:
        sign = 1
    # TODO::Try use the following regular expression (?![\.,])\D+ instead
    (degree, minute, second, frac_seconds) = re.split('\D+', dms_str, maxsplit=4)
    return sign * (int(degree) + float(minute) / 60 + float(second) / 3600 )#+ float(frac_seconds) / 36000)